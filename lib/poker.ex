defmodule Poker do
  @moduledoc """
  Documentation for Poker.
  """

  defmodule Suit do
    defstruct name: ""

    @doc """
    Create a `Suit` from a character
    """
    def from_character(character) do
      %{
        "C" => %Suit{name: "clubs"},
        "D" => %Suit{name: "diamonds"},
        "H" => %Suit{name: "hearts"},
        "S" => %Suit{name: "spades"}
      }
      |> Map.get(character)
    end
  end

  defmodule Rank do
    defstruct character: "", name: "", value: 0

    @doc """
    get the `List` of possibles Rank
    """
    def possibilites() do
      [
        %Rank{character: "2", name: "2", value: 0},
        %Rank{character: "3", name: "3", value: 1},
        %Rank{character: "4", name: "4", value: 2},
        %Rank{character: "5", name: "5", value: 3},
        %Rank{character: "6", name: "6", value: 4},
        %Rank{character: "7", name: "7", value: 5},
        %Rank{character: "8", name: "8", value: 6},
        %Rank{character: "9", name: "9", value: 7},
        %Rank{character: "T", name: "10", value: 8},
        %Rank{character: "J", name: "jack", value: 9},
        %Rank{character: "Q", name: "queen", value: 10},
        %Rank{character: "K", name: "king", value: 11},
        %Rank{character: "A", name: "ace", value: 12}
      ]
    end

    @doc """
    Create a `Rank` from the list of possibilities by character
    """
    def from_character(character) do
      Rank.possibilites() |> Enum.find(&(&1.character == character))
    end

    @doc """
    Create a `Rank` from the list of possibilities by value
    """
    def from_value(value) do
      Rank.possibilites() |> Enum.find(&(&1.value == value))
    end
  end

  defmodule Card do
    defstruct suit: %Poker.Suit{}, rank: %Poker.Rank{}

    @doc """
    Create a `Card` from a string of 2 characters
    first character is the suit
    second character is the rank
    """
    def from_string(card_str) do
      %Poker.Card{
        suit: Poker.Suit.from_character(String.at(card_str, 1)),
        rank: Poker.Rank.from_character(String.at(card_str, 0))
      }
    end
  end

  defmodule Deck do
    @doc """
    Create a `List` of cards from a `string`
    """
    def from_string(deck_str) do
      deck_str
      |> String.split(" ")
      |> Enum.map(&Card.from_string/1)
    end

    @doc """
    Transform a `List` of cards into a `Map` of <value: count(value)>)
    """
    def to_count(cards) do
      cards
      |> Enum.map(& &1.rank.value)
      |> Enum.reduce(%{}, fn x, acc -> Map.update(acc, x, 1, &(&1 + 1)) end)
    end

    @doc """
    Get the greatest duplicated rank value from a `List` of cards
    """
    def greatest_rank_duplicate(cards) do
      greatest_value = cards |> to_count |> Enum.max_by(fn {_, v} -> v end) |> elem(1)

      cards
      |> to_count
      |> Enum.filter(fn {_, v} -> v == greatest_value end)
      |> Enum.map(fn {k, _} -> k end)
      |> Enum.max()
    end

    @doc """
    Two cards of the same rank
    """
    def is_pair(cards) do
      cards |> to_count |> Enum.any?(fn {_, v} -> v >= 2 end)
    end

    @doc """
    Two different pairs
    """
    def is_two_pair(cards) do
      cards |> to_count |> Enum.filter(fn {_, v} -> v >= 2 end) |> length == 2
    end

    @doc """
    Three cards of the same rank
    """
    def is_brelan(cards) do
      cards |> to_count |> Enum.any?(fn {_, v} -> v >= 3 end)
    end

    @doc """
     All four cards of the same rank 
    """
    def is_square(cards) do
      cards |> to_count |> Enum.any?(fn {_, v} -> v >= 4 end)
    end

    @doc """
    Three of a kind with a pair. 
    """
    def is_full(cards) do
      is_two_pair(cards) and is_brelan(cards)
    end

    @doc """
    Five cards in a sequence, but not of the same suit. 
    """
    def is_straight(cards) do
      min_card = cards |> Enum.map(& &1.rank.value) |> Enum.min()

      cards |> Enum.map(& &1.rank.value) |> Enum.sort() |> Enum.map(&(&1 - min_card)) ==
        Enum.into(0..4, [])
    end

    @doc """
    Any five cards of the same suit, but not in a sequence. 
    """
    def is_flush(cards) do
      cards |> Enum.map(& &1.suit.name) |> Enum.uniq() |> length === 1
    end

    @doc """
    Five cards in a sequence, all in the same suit. 
    """
    def is_straight_flush(cards) do
      is_straight(cards) and is_flush(cards)
    end

    @doc """
    Get the score as a tuple of {family, greatest_duplicate}
    value is an `integer` that represent the familly of the card
    greatest_duplicate is an `integer` of the the greatest card of duplicates cards if no duplicate cards nil
    """
    def score(cards) do
      greatest_duplicate = cards |> greatest_rank_duplicate

      cond do
        is_straight_flush(cards) -> {:straight_flush, nil}
        is_square(cards) -> {:square, greatest_duplicate}
        is_full(cards) -> {:full, greatest_duplicate}
        is_flush(cards) -> {:flush, nil}
        is_straight(cards) -> {:straight, nil}
        is_brelan(cards) -> {:brelan, greatest_duplicate}
        is_two_pair(cards) -> {:double_pair, greatest_duplicate}
        is_pair(cards) -> {:pair, greatest_duplicate}
        true -> {:high, nil}
      end
    end

    @doc """
    Recieve an atom and return is score as an `Integer`
    """
    def familly_to_score(score_atom) do
      case score_atom do
        :high -> 0
        :pair -> 1
        :double_pair -> 2
        :brelan -> 3
        :straight -> 4
        :flush -> 5
        :full -> 6
        :square -> 7
        :straight_flush -> 8
      end
    end

    @doc """
    Recieve score1 and score2 return a `Keyword` that represent the winner
    return a `Tuple` of {score, reason}
    `score` is an Atom (:score1, :score2, :tie)
    `reason` is the number that make the game win or nil 
    """
    def winner(cards1, cards2) do
      [score1, score2] = [Deck.score(cards1), Deck.score(cards2)]
      {familly1, greatest_duplicate1} = score1
      {familly2, greatest_duplicate2} = score2

      [cards1_value, cards2_value] = [
        get_in(cards1, [Access.all(), Access.key(:rank), Access.key(:value)]),
        get_in(cards2, [Access.all(), Access.key(:rank), Access.key(:value)])
      ]

      cards1_set = MapSet.new(cards1_value)
      cards2_set = MapSet.new(cards2_value)

      score_symetric_diff =
        MapSet.union(
          MapSet.difference(cards1_set, cards2_set),
          MapSet.difference(cards2_set, cards1_set)
        )

      cond do
        familly_to_score(familly1) > familly_to_score(familly2) ->
          {:score1, familly1, nil}

        familly_to_score(familly1) < familly_to_score(familly2) ->
          {:score2, familly2, nil}

        true ->
          cond do
            familly1 in [:pair, :double_pair, :brelan, :full, :square] and
                greatest_duplicate1 != greatest_duplicate2 ->
              cond do
                greatest_duplicate1 > greatest_duplicate2 ->
                  {:score1, familly1, greatest_duplicate1}

                greatest_duplicate1 < greatest_duplicate2 ->
                  {:score2, familly2, greatest_duplicate2}
              end

            (familly1 in [
               :pair,
               :double_pair,
               :brelan,
               :full,
               :square,
               :straight,
               :flush,
               :straight_flush
             ] and
               greatest_duplicate1 == greatest_duplicate2) or familly1 == :high ->
              cond do
                MapSet.size(score_symetric_diff) === 0 ->
                  {:tie, familly1, nil}

                (score_symetric_diff |> Enum.max()) in cards1_value ->
                  {:score1, familly1, score_symetric_diff |> Enum.max()}

                (score_symetric_diff |> Enum.max()) in cards2_value ->
                  {:score2, familly2, score_symetric_diff |> Enum.max()}
              end
          end
      end
    end
  end

  defmodule Player do
    defstruct color: "", cards: []
  end

  defmodule Game do
    @doc """
    Play the game

    ## Examples
       iex> game1 = "Black: 2H 3D 5S 9C KD White: 2C 3H 4S 8C AH"
       iex> Game.play game1
       "White wins - high card: Ace"

       iex> game2 = "Black: 2H 4S 4C 3D 4H White: 2S 8S AS QS 3S"
       iex> Game.play game2
       "White wins - flush"

       iex> game3 = "Black: 2H 3D 5S 9C KD White: 2C 3H 4S 8C KH"
       iex> Game.play game3
       "Black wins - high card: 9"

       iex> game4 = "Black: 2H 3D 5S 9C KD White: 2D 3H 5C 9S KH"
       iex> Game.play game4
       "Tie"
    """
    def play(str) do
      str |> str_to_players |> format_winner |> str_winner
    end

    @doc """
    Recieve a string of (players, decks) then transform it into a list of `Player`
    """
    def str_to_players(str) do
      str
      |> String.split(~r{\s?(\w)*:\s}, include_captures: true, trim: true)
      |> Enum.chunk_every(2)
      |> Enum.map(fn [color_str, deck_str] ->
        %Player{
          color: color_str |> String.replace(": ", "") |> String.trim(),
          cards: Deck.from_string(deck_str)
        }
      end)
    end

    @doc """
    Recieve a List of 2 players
    Get the { color, score } of the winner or : `:tie` if no winner
    """
    def format_winner(players) do
      [color1, color2] = get_in(players, [Access.all(), Access.key(:color)])
      [cards1, cards2] = get_in(players, [Access.all(), Access.key(:cards)])

      case Deck.winner(cards1, cards2) do
        {:score1, familly, reason} -> {color1, familly, reason}
        {:score2, familly, reason} -> {color2, familly, reason}
        {:tie, _, _} -> :tie
      end
    end

    @doc """
    Recieve a :tie
    return "Tie"
    """
    def str_winner(:tie), do: "Tie"

    @doc """
    Recieve a tuple of color score 
    return "formatted winner"
    """
    def str_winner({color, familly, card_value}) do
      color_str = color |> String.capitalize()
      familly_str = familly |> Atom.to_string() |> String.replace("_", " ")

      card_str =
        cond do
          card_value == nil -> ""
          true -> " card: #{Rank.from_value(card_value).name |> String.capitalize()}"
        end

      "#{color_str} wins - #{familly_str}#{card_str}"
    end
  end
end
