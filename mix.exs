defmodule Poker.MixProject do
  use Mix.Project

  def project do
    [
      app: :poker,
      version: "0.1.0",
      elixir: "~> 1.8",
      escript: escript(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "Poker App",
      source_url: "https://bitbucket.org/edil3ra/poker_bettyblocks/src/master/",
      homepage_url: "https://bitbucket.org/edil3ra/poker_bettyblocks/src/master/",
      docs: [
        extras: ["README.md"]
      ],

      # Test
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.20.2", only: :dev, runtime: false},
      {:excoveralls, "~> 0.10", only: :test}
    ]
  end

  defp escript do
    [main_module: Poker.CLI]
  end
  
end
