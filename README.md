# Poker
    
    Create an application that compare 2 hands of poker and get the back the winner hand


## Installation
    
    mix deps.get
    
## Generate doc
    
    mix docs

## Generate test coverage
    
    mix coveralls.html

## build the app
    
    mix escript.build

## Launch
    
    I made a small CLI to test the application, it just take a string as an entry and Passed it to the Game.play function,It doesn't check if the string is correct

### How to use
    
    ./poker "Black: 2H 3D 5S 9C KD White: 2C 3H 4S 8C AH"
    
    ./poker "Black: 2H 4S 4C 3D 4H White: 2S 8S AS QS 3S"

    ./poker "Black: 2H 3D 5S 9C KD White: 2C 3H 4S 8C KH"

    ./poker "Black: 2H 3D 5S 9C KD White: 2D 3H 5C 9S KH"


