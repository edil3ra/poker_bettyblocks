defmodule PokerTest do
  alias Poker.{Suit, Rank, Deck, Game}
  use ExUnit.Case
  doctest Game

  describe "Poker.Suit Module" do
    test "from_chracter return a Suit `Struct`" do
      assert Suit.from_character("C") == %Poker.Suit{name: "clubs"}
      assert Suit.from_character("D") == %Poker.Suit{name: "diamonds"}
      assert Suit.from_character("H") == %Poker.Suit{name: "hearts"}
      assert Suit.from_character("S") == %Poker.Suit{name: "spades"}
    end
  end

  describe "Poker.Rank Module" do
    test "possibilites return the given list" do
      assert Rank.possibilites() === [
               %Rank{character: "2", name: "2", value: 0},
               %Rank{character: "3", name: "3", value: 1},
               %Rank{character: "4", name: "4", value: 2},
               %Rank{character: "5", name: "5", value: 3},
               %Rank{character: "6", name: "6", value: 4},
               %Rank{character: "7", name: "7", value: 5},
               %Rank{character: "8", name: "8", value: 6},
               %Rank{character: "9", name: "9", value: 7},
               %Rank{character: "T", name: "10", value: 8},
               %Rank{character: "J", name: "jack", value: 9},
               %Rank{character: "Q", name: "queen", value: 10},
               %Rank{character: "K", name: "king", value: 11},
               %Rank{character: "A", name: "ace", value: 12}
             ]
    end

    test "from_character return the correct Rank `Struct`" do
      assert Rank.from_character("2") == %Rank{character: "2", name: "2", value: 0}
      assert Rank.from_character("A") == %Rank{character: "A", name: "ace", value: 12}
    end

    test "from_value return the correct Rank `Struct`" do
      assert Rank.from_value(0) == %Rank{character: "2", name: "2", value: 0}
      assert Rank.from_value(12) == %Rank{character: "A", name: "ace", value: 12}
    end
  end

  describe "Poker.Deck Module" do
    @cards1 "2H 3D 5S 9C KD"
    @cards2 "2C 3H 4S 8C AH"
    @cards3 "2H 4S 4C 3D 4H"
    @cards4 "5H 4S 4C 5D 7H"
    @cards_square "JH JD JS JC 7D"
    @cards_square_2 "AH AD AS AC 7D"
    @cards_full "TH TD TS 9C 9D"
    @cards_full_2 "JH JD JS 9C 9D"
    @cards_flush "4C JC 8C 2C 9C"
    @cards_flush_2 "AC JC 8C 2C 9C"
    @cards_straight "9C 8D 7S 6D 5H"
    @cards_straight_2 "TC 9D 8S 7D 6H"
    @cards_straight_flush "8C 7C 6C 5C 4C"
    @cards_straight_flush_2 "9C 8C 7C 6C 5C"
    @cards_brelan "7C 7D 7S KC 3D"
    @cards_brelan_2 "8C 8D 8S KC 3D"
    @cards_two_pair "4C 4S 3C 3D QC"
    @cards_two_pair_2 "AC AS 3C 3D QC"
    @cards_two_pair_3 "AH AD 3H 3S KD"
    @cards_pair "KH KC 8C 4S 7H"
    @cards_pair_2 "AH AC 8C 4S 7H"
    @cards_pair_3 "AD AS KC 4D 7S"
    @cards_high "2H 3H 5C 9S JH"
    @cards_high_2 "AH 3H 5C 9S 2C"

    test "from_string return a List of cards" do
      assert Deck.from_string("2H 3D 5S 9C JD") === [
               %Poker.Card{
                 rank: %Poker.Rank{character: "2", name: "2", value: 0},
                 suit: %Poker.Suit{name: "hearts"}
               },
               %Poker.Card{
                 rank: %Poker.Rank{character: "3", name: "3", value: 1},
                 suit: %Poker.Suit{name: "diamonds"}
               },
               %Poker.Card{
                 rank: %Poker.Rank{character: "5", name: "5", value: 3},
                 suit: %Poker.Suit{name: "spades"}
               },
               %Poker.Card{
                 rank: %Poker.Rank{character: "9", name: "9", value: 7},
                 suit: %Poker.Suit{name: "clubs"}
               },
               %Poker.Card{
                 rank: %Poker.Rank{character: "J", name: "jack", value: 9},
                 suit: %Poker.Suit{name: "diamonds"}
               }
             ]

      assert Deck.from_string("2C 3H 4S 8C AH") === [
               %Poker.Card{
                 rank: %Poker.Rank{character: "2", name: "2", value: 0},
                 suit: %Poker.Suit{name: "clubs"}
               },
               %Poker.Card{
                 rank: %Poker.Rank{character: "3", name: "3", value: 1},
                 suit: %Poker.Suit{name: "hearts"}
               },
               %Poker.Card{
                 rank: %Poker.Rank{character: "4", name: "4", value: 2},
                 suit: %Poker.Suit{name: "spades"}
               },
               %Poker.Card{
                 rank: %Poker.Rank{character: "8", name: "8", value: 6},
                 suit: %Poker.Suit{name: "clubs"}
               },
               %Poker.Card{
                 rank: %Poker.Rank{character: "A", name: "ace", value: 12},
                 suit: %Poker.Suit{name: "hearts"}
               }
             ]
    end

    test "to_count return a map of rank count" do
      assert Deck.to_count(@cards1 |> Deck.from_string()) === %{
               0 => 1,
               1 => 1,
               3 => 1,
               7 => 1,
               11 => 1
             }

      assert Deck.to_count(@cards3 |> Deck.from_string()) === %{0 => 1, 1 => 1, 2 => 3}
      assert Deck.to_count(@cards4 |> Deck.from_string()) === %{2 => 2, 3 => 2, 5 => 1}
    end

    test "greatest_rank_duplicate return the biggest duplicate number from a List of cards" do
      assert Deck.greatest_rank_duplicate(@cards3 |> Deck.from_string()) === 2
      assert Deck.greatest_rank_duplicate(@cards4 |> Deck.from_string()) === 3
    end

    test "is_pair return true when deck is a pair otherwise false" do
      assert Deck.is_pair(@cards_pair |> Deck.from_string()) === true
      assert Deck.is_pair(@cards_straight |> Deck.from_string()) === false
    end

    test "is_two_pair return true when deck has 2 pairs otherwise false" do
      assert Deck.is_two_pair(@cards_two_pair |> Deck.from_string()) === true
      assert Deck.is_two_pair(@cards_pair |> Deck.from_string()) === false
    end

    test "is_brelan return true when deck is a brelan otherwise false" do
      assert Deck.is_brelan(@cards_brelan |> Deck.from_string()) === true
      assert Deck.is_brelan(@cards_pair |> Deck.from_string()) === false
    end

    test "is_square return true when deck is a square otherwise false" do
      assert Deck.is_square(@cards_square |> Deck.from_string()) === true
      assert Deck.is_square(@cards_pair |> Deck.from_string()) === false
    end

    test "is_full return true when deck is a full otherwise false" do
      assert Deck.is_full(@cards_full |> Deck.from_string()) === true
      assert Deck.is_full(@cards_pair |> Deck.from_string()) === false
    end

    test "is_straight return true when deck is a straight otherwise false" do
      assert Deck.is_straight(@cards_straight |> Deck.from_string()) === true
      assert Deck.is_straight(@cards_pair |> Deck.from_string()) === false
    end

    test "is_flush return true when deck is a flush otherwise false" do
      assert Deck.is_flush(@cards_flush |> Deck.from_string()) === true
      assert Deck.is_flush(@cards_pair |> Deck.from_string()) === false
    end

    test "is_flush_straight return true when deck is a flush straight otherwise false" do
      assert Deck.is_straight_flush(@cards_straight_flush |> Deck.from_string()) === true
      assert Deck.is_straight_flush(@cards_pair |> Deck.from_string()) === false
    end

    test "score return the correct {family, greatest, greatest_dulpicate} value" do
      assert Deck.score(@cards_straight_flush |> Deck.from_string()) === {:straight_flush, nil}
      assert Deck.score(@cards_square |> Deck.from_string()) === {:square, 9}
      assert Deck.score(@cards_full |> Deck.from_string()) === {:full, 8}
      assert Deck.score(@cards_flush |> Deck.from_string()) === {:flush, nil}
      assert Deck.score(@cards_straight |> Deck.from_string()) === {:straight, nil}
      assert Deck.score(@cards_brelan |> Deck.from_string()) === {:brelan, 5}
      assert Deck.score(@cards_two_pair |> Deck.from_string()) === {:double_pair, 2}
      assert Deck.score(@cards_pair |> Deck.from_string()) === {:pair, 11}
      assert Deck.score(@cards_high |> Deck.from_string()) === {:high, nil}
    end

    test "familly_score return the value from atom" do
      assert Deck.familly_to_score(:high) === 0
      assert Deck.familly_to_score(:pair) === 1
      assert Deck.familly_to_score(:double_pair) === 2
      assert Deck.familly_to_score(:brelan) === 3
      assert Deck.familly_to_score(:straight) === 4
      assert Deck.familly_to_score(:flush) === 5
      assert Deck.familly_to_score(:full) === 6
      assert Deck.familly_to_score(:square) === 7
      assert Deck.familly_to_score(:straight_flush) === 8
    end

    test "winner return the corect tuple" do
      cards_square = @cards_square |> Deck.from_string()
      cards_square_2 = @cards_square_2 |> Deck.from_string()
      cards_full = @cards_full |> Deck.from_string()
      cards_full_2 = @cards_full_2 |> Deck.from_string()
      cards_brelan = @cards_brelan |> Deck.from_string()
      cards_brelan_2 = @cards_brelan_2 |> Deck.from_string()
      cards_two_pair = @cards_two_pair |> Deck.from_string()
      cards_two_pair_2 = @cards_two_pair_2 |> Deck.from_string()
      cards_two_pair_3 = @cards_two_pair_3 |> Deck.from_string()
      cards_pair = @cards_pair |> Deck.from_string()
      cards_pair_2 = @cards_pair_2 |> Deck.from_string()
      cards_pair_3 = @cards_pair_3 |> Deck.from_string()
      cards_high = @cards_high |> Deck.from_string()
      cards_high_2 = @cards_high_2 |> Deck.from_string()
      cards_flush = @cards_flush |> Deck.from_string()
      cards_flush_2 = @cards_flush_2 |> Deck.from_string()
      cards_straight = @cards_straight |> Deck.from_string()
      cards_straight_2 = @cards_straight_2 |> Deck.from_string()
      cards_straight_flush = @cards_straight_flush |> Deck.from_string()
      cards_straight_flush_2 = @cards_straight_flush_2 |> Deck.from_string()


      assert Deck.winner(cards_pair, cards_square) === {:score2, :square, nil}
      assert Deck.winner(cards_square, cards_pair)=== {:score1, :square, nil}
      
      assert Deck.winner(cards_pair, cards_pair_2) === {:score2, :pair, 12}
      assert Deck.winner(cards_pair_2, cards_pair) === {:score1, :pair, 12}
      assert Deck.winner(cards_pair_2, cards_pair_3) === {:score2, :pair, 11}
      assert Deck.winner(cards_pair_3, cards_pair_2) === {:score1, :pair, 11}
      assert Deck.winner(cards_pair_2, cards_pair_2) === {:tie, :pair, nil}

      assert Deck.winner(cards_two_pair, cards_two_pair_2) === {:score2, :double_pair, 12}
      assert Deck.winner(cards_two_pair_2, cards_two_pair) === {:score1, :double_pair, 12}
      assert Deck.winner(cards_two_pair_2, cards_two_pair_3) === {:score2, :double_pair, 11}
      assert Deck.winner(cards_two_pair_3, cards_two_pair_2) === {:score1, :double_pair, 11}
      assert Deck.winner(cards_two_pair_2, cards_two_pair_2) === {:tie, :double_pair, nil}

      assert Deck.winner(cards_brelan, cards_brelan_2) === {:score2, :brelan, 6}
      assert Deck.winner(cards_brelan_2, cards_brelan) === {:score1, :brelan, 6}
      assert Deck.winner(cards_brelan, cards_brelan) === {:tie, :brelan, nil}

      assert Deck.winner(cards_square, cards_square_2) === {:score2, :square, 12}
      assert Deck.winner(cards_square_2, cards_square) === {:score1, :square, 12}
      assert Deck.winner(cards_square, cards_square) === {:tie, :square, nil}

      assert Deck.winner(cards_full, cards_full_2) === {:score2, :full, 9}
      assert Deck.winner(cards_full_2, cards_full) === {:score1, :full, 9}
      assert Deck.winner(cards_full, cards_full) === {:tie, :full, nil}

      assert Deck.winner(cards_high, cards_high_2) === {:score2, :high, 12}
      assert Deck.winner(cards_high_2, cards_high) === {:score1, :high, 12}
      assert Deck.winner(cards_high, cards_high) === {:tie, :high, nil}

      assert Deck.winner(cards_flush, cards_flush_2) === {:score2, :flush, 12} 
      assert Deck.winner(cards_flush_2, cards_flush) === {:score1, :flush, 12}
      assert Deck.winner(cards_flush, cards_flush) === {:tie, :flush, nil}

      assert Deck.winner(cards_flush, cards_flush_2) === {:score2, :flush, 12} 
      assert Deck.winner(cards_flush_2, cards_flush) === {:score1, :flush, 12}
      assert Deck.winner(cards_flush, cards_flush) === {:tie, :flush, nil}

      assert Deck.winner(cards_straight_flush, cards_straight_flush_2) === {:score2, :straight_flush, 7}
      assert Deck.winner(cards_straight_flush_2, cards_straight_flush) === {:score1, :straight_flush, 7}
      assert Deck.winner(cards_straight_flush, cards_straight_flush) === {:tie, :straight_flush, nil}

    end
  end
end
